<?php
class BlogPost extends DataObject {
    private static $db = array(
        'Name' => 'Varchar',
        'Details' => 'Varchar',
    );
    private static $has_one = array(
        'Category' => 'BlogCategory'
    );    
}